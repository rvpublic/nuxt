import { defineStore } from 'pinia' // нужно но вроде необязательно

export const usePaysStore = defineStore('pays', {
  // так задаем название, для автори
  // зации ипользую имя Auth useAuthStore
  state: () => {
    return {
      // здесь одно свойство
      // методов нет в pinia там где нужно будешь менять напрямую - смотри файл app.vue
      pays: [
        { id: 3, name: 'Киви', price: 388 },
        { id: 4, name: 'Арбуз', price: 99 },
      ] as app.Pay[], // так показываю акой тип
    }
  },
})
