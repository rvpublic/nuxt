/**
 * @type {import('prettier').Options}
 */

// prettier.config.js or .prettierrc.js
module.exports = {
  plugins: [],
  semi: false,
  printWidth: 100,
  singleQuote: true,
  // arrowParens: 'avoid'
}
