// eslint-disable-next-line no-undef
module.exports = {
  root: true,
  extends: ['@nuxt/eslint-config'],
  rules: {
    'vue/max-attributes-per-line': ['error', { singleline: 6, multiline: 1 }], // 6 атрибутов в строке если поставить поменьше то будет складываться
    'vue/multi-word-component-names': 0,
  },
}
