declare namespace app {
  // можно и без этого, но лучше задавать вложенность я назвал app
  interface Pay {
    id: number
    name: string
    price: number
  }

  interface Category {
    id: number
    title: string
    pays: Pay[] // внутри категории есть платежи
  }
}
